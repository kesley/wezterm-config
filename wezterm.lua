local wezterm = require("wezterm")
local config = wezterm.config_builder()

-- Font
config.font_size = 13
config.font = wezterm.font("JetBrains Mono", { weight = "Bold" })

-- Padding
config.window_padding = {
	left = 0,
	right = 0,
	top = 0,
	bottom = 0,
}

-- Opacity
config.window_background_opacity = 0.7
config.text_background_opacity = 0.5

-- Colorscheme
config.color_scheme = "Catppuccin Mocha"

return config
